package de.beocode.bestbefore.repositories

import androidx.datastore.core.DataStore
import de.beocode.bestbefore.data.AppData
import de.beocode.bestbefore.data.FoodItem
import kotlinx.coroutines.flow.Flow

class AppDataRepository(private val appDataStore: DataStore<AppData>) {

    fun getData(): Flow<AppData> {
        return appDataStore.data
    }

    suspend fun setExpireWarning(days: Int) {
        appDataStore.updateData { AppData ->
            AppData.copy(
                warningDays = days
            )
        }
    }

    suspend fun addFood(foodItem: FoodItem) {
        appDataStore.updateData { AppData ->
            val list = AppData.foodList.toMutableList()
            list.add(foodItem)
            list.sortBy { it.ts }
            AppData.copy(
                foodList = list
            )
        }
    }

    suspend fun updateFood(index: Int, foodItem: FoodItem) {
        appDataStore.updateData { AppData ->
            val list = AppData.foodList.toMutableList()
            list[index] = foodItem
            list.sortBy { it.ts }
            AppData.copy(
                foodList = list
            )
        }
    }

    suspend fun deleteFood(index: Int) {
        appDataStore.updateData { AppData ->
            val list = AppData.foodList.toMutableList()
            list.removeAt(index)
            AppData.copy(
                foodList = list
            )
        }
    }

    suspend fun addName(name: String) {
        appDataStore.updateData { AppData ->
            val list = AppData.nameSuggestions.toMutableList()
            list.add(name)
            AppData.copy(
                nameSuggestions = list
            )
        }
    }

    suspend fun addDestination(place: String) {
        appDataStore.updateData { AppData ->
            val list = AppData.placeSuggestions.toMutableList()
            list.add(place)
            AppData.copy(
                placeSuggestions = list
            )
        }
    }

    suspend fun importData(appData: AppData) {
        appDataStore.updateData {
            appData
        }
    }

    suspend fun clearData() {
        appDataStore.updateData {
            AppData()
        }
    }
}