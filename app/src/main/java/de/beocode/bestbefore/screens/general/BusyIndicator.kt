package de.beocode.bestbefore.screens.general

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun BusyIndicator(isBusy: Boolean) {
    if (isBusy) {
        LinearProgressIndicator(
            modifier = Modifier
                .height(2.dp)
                .fillMaxWidth()
        )
    } else {
        Spacer(modifier = Modifier.height(2.dp))
    }
}