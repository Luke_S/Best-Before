package de.beocode.bestbefore.utils

import android.content.Context
import de.beocode.bestbefore.R

private const val WEEK = 7
private const val MONTH = 31
private const val YEAR = 365

fun convertDiffToText(context: Context, diff: Int): String {
    return if (-1 < diff && diff < 1) {
        context.getString(R.string.expires_today)
    } else if (diff < 0) {
        context.getString(R.string.expired, diffToTimeAgo(context, -diff))
    } else {
        context.getString(R.string.expires_in, diffToTimeAgo(context, diff))
    }
}

private fun diffToTimeAgo(context: Context, diff: Int): String {
    return if (diff < WEEK) {
        context.resources.getQuantityString(R.plurals.day, diff, diff)
    } else if (diff < MONTH) {
        context.resources.getQuantityString(R.plurals.week, diff / WEEK, diff / WEEK)
    } else if (diff < YEAR) {
        context.resources.getQuantityString(R.plurals.month, diff / MONTH, diff / MONTH)
    } else {
        val years = diff / YEAR
        val rest = diff - (years * YEAR)
        if (rest >= MONTH) {
            context.getString(
                R.string.and,
                context.resources.getQuantityString(R.plurals.year, years, years),
                context.resources.getQuantityString(R.plurals.month, rest / MONTH, rest / MONTH)
            )
        } else {
            context.resources.getQuantityString(R.plurals.year, years, years)
        }
    }
}