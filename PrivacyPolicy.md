# Privacy Policy
Best-Before was built as a free and open source application (app). This app is provided free of charge and is intended for use as is.  
This page is intended to inform you of the policies regarding the collection, use and disclosure of personal information should you choose to use this app.  
By using this app, you consent to the collection and use of information under this policy.

## Data we collect
We, the programmers of Best-Before, do not collect any type of data from users of this app.  

## Data collected by others
Use of Best-Before may result in one or more of the following third party organisations collecting data from you. Please check their privacy policy for more information:
- [F-Droid](https://f-droid.org/en/about/), store where the app is published
- [Google PlayStore](https://policies.google.com/privacy), store where the app is published

In addition, in case you take part in the development of the app:
- [Codeberg](https://codeberg.org/codeberg/org/src/branch/main/PrivacyPolicy.md), whose platform we use to facilitate development of the app

In addition, in case you take part in the translation of the app:
- [Weblate](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md), whose platform we use to facilitate translation of the app
- [POEditor](https://poeditor.com/terms/privacy), whose platform we have used to facilitate translation of the app

## Policy Updates
This privacy policy may get modified from time to time.  
This policy is effective as of 2022-05-16.

## Contact
Questions and suggestions about the Weather app can be sent to us via [email](mailto:best-before@beocode.eu) or through the [ticket system](https://codeberg.org/BeoCode/Best-Before/issues).